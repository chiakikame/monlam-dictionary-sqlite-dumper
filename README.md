This application is for dumping content of MLDic.ml inside Android app `Nonlam Tibetan-English dictionary`.

## Usage

First, download `MLDic.ml` from https://github.com/iamironrabbit/monlam-dictionary. It's in `app/src/main/assets/`.

Then, run this application with

```sh
node index.js /path/to/MLDic.ml /output/directory/
```
And the application will dump the database and transform them into tab-delimited files: `tben.txt` and `tbtb.txt`.
