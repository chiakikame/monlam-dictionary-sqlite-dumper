const sqlite = require('sqlite');
const fs = require('fs');
const path = require('path');
const logger = require('./logger')('transformer');

module.exports = function runTransformer(srcDbPath, destDir) {
  let db = null;
  sqlite.open(srcDbPath, { Promise })
    .then(function dbOpened(_db) {
      logger.info('Database opened');
      db = _db;
    }).then(function handleTbEn() {
      return dumpDbContent(db, 'tben', destDir);
    }).then(function handleTbTb() {
      return dumpDbContent(db, 'tbtb', destDir);
    }).catch(function dbError(err) {
      logger.error(`While processing database ${srcDbPath}: ${err.message}`);
    });
};

function dumpDbContent(db, dbName, destDir) {
  const targetFile = path.join(destDir, `${dbName}.txt`);
  logger.info(`Dumping table ${dbName} to ${targetFile}`);
  
  const wStream = fs.createWriteStream(targetFile);
  wStream.write('word\tdefinition\n');
  return db.each(`SELECT * FROM ${dbName}`, function dbRow(err, row) {
    if (err) {
      logger.error(`While dumping ${dbName}: ${err.message}`);
    } else {
      wStream.write(`${row.word}\t${row.definition}\n`);
    }
  }).then(function dbEnd(count) {
    logger.info(`${count} entries processed`);
  });
}
