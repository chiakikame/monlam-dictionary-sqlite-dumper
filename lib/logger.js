const levels = {
  DEBUG: 1, INFO: 1, ERROR: 1
};
const logLevel = isLogLevel(process.env.LOGLEVEL) ?
  process.env.LOGLEVEL : 'INFO';

function isLogLevel(level) {
  return levels[level] === 1;
}

function doNothing() {}

/* eslint-disable no-console */
function log(tag, level) {
  return function _log(msg) {
    const dtString = new Date().toISOString();
    console.log(`[${tag}] ${dtString} ${level} | ${msg}`);
  };
}

function makeLogFunction(tag, level) {
  if (logLevel === level) {
    return log(tag, level);
  } else {
    return doNothing;
  }
}

module.exports = function makeLogger(tag) {
  return {
    debug: makeLogFunction(tag, 'DEBUG'),
    info: makeLogFunction(tag, 'INFO'),
    error: makeLogFunction(tag, 'ERROR')
  };
};
