const fs = require('fs');
const path = require('path');
const logger = require('./logger')('ensure-dir');

module.exports = function ensureDestDir(destDir) {
  logger.debug(`Ensuring ${destDir}`);
  return new Promise(function _ensureDestDir(resolve, reject) {
    fs.stat(destDir, function(err, stat) {
      if (err) {
        if (err.code === 'ENOENT') {
          logger.debug(`Entry ${destDir} not found, trying to make it`);
          ensureDestDir(path.dirname(destDir))
            .then(function parentEnsured() {
              fs.mkdir(destDir, function mkDirResult(err) {
                if (err) {
                  reject(err);
                } else {
                  resolve(destDir);
                }
              });
            }).catch(function ensureParentFailed(err) {
              reject(err);
            });
        } else {
          logger.debug(`Cannot stat ${destDir}`);
          reject(err);
        }
      } else {
        // Check if it's a directory
        if (stat.isDirectory()) {
          logger.debug(`Ensured: ${destDir}`);
          resolve(destDir);
        } else {
          logger.debug(`Ensure failed: ${destDir} is a file`);
          reject(new Error('${destDir} should be a directory'));
        }
      }
    });
  });
};
