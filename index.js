const path = require('path');
const os = require('os');
const logger = require('./lib/logger')('main');
const ensureDestDir = require('./lib/ensure-dest-dir');
const runTransformer = require('./lib/run-transformer');

//
// Application settings
//

const sourcePath = process.argv[2] ? process.argv[2] : defaultDataPath();
const destDir = process.argv[3] ? process.argv[3] : defaultOutputDir();

function defaultDataPath() {
  return path.join(__dirname, 'data', 'MLDic.ml');
}

function defaultOutputDir() {
  return path.join(os.tmpdir(), 'monlam-dict-dump');
}

logger.info(`Source database: ${sourcePath}`);
logger.info(`Destination directory: ${destDir}`);

//
// Actual processing code starts here
//
ensureDestDir(destDir).then(function destEnsured(destDir) {
  runTransformer(sourcePath, destDir);
}).catch(function cannotEnsureDestDir(err) {
  logger.error(`Cannot ensure destination dir ${destDir}: ${err.message}`);
});
